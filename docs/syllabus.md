# Foundations in Computational Skills

Adam Labadorf
04/07/2017

## Course description

This short course covers topics in practical use of computational resources in
the implementation of analysis, with a particular focus on bioinformatics.  The
course is split into nine modules. The first four modules covers basic to
intermediate command line tools, bash scripting, cluster utilization tools, and
strategies for effectively interfacing with the BU linux cluster systems.  The
next module is a concentrated course on language-independent strategies for
writing readable, well organized, and correct code, including a short
introduction to formal software testing methodologies. The remaining modules
focus on tools and strategies to write, record, and organize code projects
such that they are transparent, replicable, and shareable.

Each module is delivered in a two hour block, including lecture and hands-on
components depending on the module.

## Condensed Syllabus

1. Week 1
    * Module 1: SCC and basic command line usage
    * Module 2: Intermediate command line usage and tools
    * Module 3: Shell scripting
2. Week 2
    * Module 4: Cluster computing (SGE)
    * Module 5: Common Coding Conventions and Testing
    * Module 6: git, github, bitbucket
3. Week 3
    * Module 7: anaconda and miniconda
    * Module 8: snakemake
    * Module 9: jupyter notebooks, Rstudio and shiny apps


## Detailed Syllabus

1. Week 1
    * Module 1: SCC and basic command line usage
        - terminal programs, ssh and ssh clients, qrsh
        - basic tools: ls, cd, man, mkdir, cat, head, tail, pushd, popd
        - basic concepts: pipe, redirection, stdin, stderr, stdout
        - text editors: nano, emacs, vim
        - Activity: Terminal Quest
    * Module 2: Intermediate command line usage and tools
        - GNU screen
        - intermediate tools: find, xargs, watch, wc, grep, sed, ln, diff
        - file and directory modes/permissions
        - troubleshooting: echo, which, $PATH, du, quota
    * Module 3: Shell scripting
        - shell script format: shabang line, executable file mode, invocation
        - defining variables, variable expansion, and advanced variable operations
        - passing command line arguments: $1, shift, $#
        - conditionals and loop constructs
2. Week 2
    * Module 4: Cluster computing (SGE)
        - cluster basics: head vs compute nodes, cores/slots, resource limits
        - sge commands: qsub, qstat, qrsh
        - troubleshooting: .e* and .o*, qdel, qstat -j
    * Module 5: Common Coding Conventions and Testing
        - good practices: code readability, commenting conventions, indentation,
          white space, etc
        - modularity strategies
        - identifying and using the right tool
        - testing: unit tests, testing frameworks, test driven development
    * Module 6: git, github, bitbucket
        - git basics: clone, status, add, commit, pull, push
        - intermediate: merge, resolving conflicts, reverting changes
        - github and bitbucket, forks, and pull requests
        - common and suggested practices for git repositories
        - Activity: git battle

3. Week 3
    * Module 7: anaconda and docker
        - conda concepts: environments, packages, channels
        - managing and recording conda environments
        - what to do if the software you need does not have a conda package
        - docker intro
    * Module 8: snakemake and nextflow
        - workflow management software concepts
        - snakemake concepts and the workflow DAG
        - writing rules
        - running snakemake locally and on the cluster
        - suggested practices to make workflows replicable
        - troubleshooting
        - Activity: some snakemake workflow example
    * Module 9: jupyter notebooks and Rstudio
        - jupyter notebooks, python and R kernels
        - plotting in jupyter notebooks with %pylab inline
        - Rstudio
